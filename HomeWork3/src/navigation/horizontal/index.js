export default [
  {
    header: 'Pages',
    icon: 'FileIcon',
    children: [
      {
        title: 'Dashboard',
        route: 'dashboard',
        icon: 'HomeIcon',
      },
      {
        title: 'Home',
        route: 'home',
        icon: 'HomeIcon',
      },
      {
        title: 'Second Page',
        route: 'second-page',
        icon: 'FileIcon',
      },
      {
        title: 'Vee Validation',
        route: 'validation',
        icon: 'FeatherIcon',
      },
    ],
  },
]
