export default [
  {
    title: 'Dashboard',
    route: 'dashboard',
    icon: 'HomeIcon',
  },
  {
    title: 'Products',
    route: 'products',
    icon: 'CodesandboxIcon',
  },
  {
    title: 'Articles',
    route: 'articles',
    icon: 'FileTextIcon',
  },
  {
    title: 'Vee Validation',
    route: 'validation',
    icon: 'FeatherIcon',
  },
  {
    title: 'Figma',
    route: 'figma',
    icon: 'FigmaIcon',
  },
]
