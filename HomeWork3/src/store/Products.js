/* eslint-disable no-return-await */
/* eslint-disable no-return-assign */
import {
  deletePro, get, pushPro, updatePro,
} from '@/services/product'

export default {
  namespaced: true,
  state: {
    Products: [],
  },
  mutations: {
    setProducts: (state, data) => (state.Products = data),
    pushProduct: (state, data) => state.Products.push(data),
    updateProduct: (state, data) => (state.Products[data.index] = data.data),
    deleteProduct: (state, index) => state.Products.splice(index, 1),
  },
  actions: {
    loadProducts: async (context, data) => await get(data)
      .then(e => {
        context.commit('setProducts', e.data)
        return e.data
      })
      .catch(error => console.error(error)),
    pushPro: async (context, data) => await pushPro(data).then(e => {
      context.commit('pushProduct', e)
      return e
    }),
    updatePro: async (context, payload) => await updatePro(payload.data, payload.index).then(e => {
      const o = { index: payload.index, data: e }
      context.commit('updateProduct', o)
      return o
    }),
    deletePro: async (context, index) => await deletePro(index).then(e => {
      context.commit('deleteProduct', index)
      return e
    }),
  },
  getters: {},
}
