export default {
  namespaced: true,
  state: {
    listBreadCrumb: [
      {
        label: 'Home',
      },
      {
        label: 'Products',
      },
      {
        label: 'Articles',
      },
    ],
  },
  mutations: {},
  actions: {},
  getters: {},
}
