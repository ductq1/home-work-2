/* eslint-disable no-return-assign */
import { get } from '@/services/article'

export default {
  namespaced: true,
  state: {
    Articles: [],
  },
  mutations: {
    setArticles: (state, data) => (state.Articles = data),
  },
  actions: {
    // eslint-disable-next-line no-return-await
    loadArticles: async (context, data) => await get(data)
      .then(e => {
        context.commit('setArticles', e.data)
        return e.data
      })
      .catch(error => console.error(error)),
  },
  getters: {},
}
