import Vue from "vue";
import Vuex from "vuex";
import BreadCrumb from "./components/BreadCrumb";
import Product from "./Products";
import Article from "./Articles";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    BreadCrumb,
    Product,
    Article,
  },
});
