import { BreadCrumb } from "@/types/index";

interface typeOfState {
  listBreadCrumb: BreadCrumb[];
}

export default {
  namespaced: true,
  state: {
    listBreadCrumb: [
      {
        label: "Home",
      },
      {
        label: "Products",
      },
      {
        label: "Articles",
      },
    ],
  } as typeOfState,
  mutations: {},
  actions: {},
  getters: {},
};
