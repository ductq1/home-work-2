import { get, article } from "@/services/article";
import {} from "vuex";

interface typeOfState {
  Articles: article[];
}

export default {
  namespaced: true,
  state: {
    Articles: [],
  } as typeOfState,
  mutations: {
    setArticles: (state: any, data: any) => (state.Articles = data),
  },
  actions: {
    loadArticles: async (context: any, data: any) => {
      return await get(data)
        .then((e: any) => {
          context.commit("setArticles", e.data);
          return e.data;
        })
        .catch((error) => console.error(error));
    },
  },
  getters: {},
};
