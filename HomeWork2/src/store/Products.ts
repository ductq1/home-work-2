import { get, product } from "@/services/product";
import {} from "vuex";

interface typeOfState {
  Products: product[];
}

export default {
  namespaced: true,
  state: {
    Products: [],
  } as typeOfState,
  mutations: {
    setProducts: (state: any, data: any) => (state.Products = data),
  },
  actions: {
    loadProducts: async (context: any, data: any) => {
      return await get(data)
        .then((e: any) => {
          context.commit("setProducts", e.data);
          return e.data;
        })
        .catch((error) => console.error(error));
    },
  },
  getters: {},
};
